Script to simulate site-percolation on a square lattice.
(forest fire algorithm).

- Produce plots and animations
- Produce statistics
- Uses numba for speeding up the slow but clean algorithm.